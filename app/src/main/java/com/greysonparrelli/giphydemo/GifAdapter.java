package com.greysonparrelli.giphydemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.greysonparrelli.giphydemo.model.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * And adapter that binds {@link Image} models to actual views.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class GifAdapter extends RecyclerView.Adapter<GifAdapter.GifViewHolder> {

    /** The list of {@link Image}s to be bound to the views. */
    private List<Image> mImages;

    /** How wide the view is supposed to be. */
    private final int mColumnWidth;

    /** A callback that is triggered when each GIF is interacted with. */
    private final ItemCallback mItemCallback;

    /**
     * @param columnWidth How wide the views should be.
     * @param callback The callback that will be triggered when a GIF is interacted with.
     */
    public GifAdapter(int columnWidth, ItemCallback callback) {
        mImages = new ArrayList<>();
        mColumnWidth = columnWidth;
        mItemCallback = callback;
        setHasStableIds(true);
    }

    @Override
    public GifViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gif, parent, false);
        return new GifViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GifViewHolder holder, int position) {
        holder.bind(mImages.get(position), position);
    }

    @Override
    public void onViewRecycled(GifViewHolder holder) {
        holder.recycle();
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    @Override
    public long getItemId(int position) {
        return mImages.get(position).url.hashCode();
    }

    public void clearImages() {
        mImages.clear();
        notifyDataSetChanged();
    }

    public void addImages(List<Image> images) {
        mImages.addAll(images);
        notifyDataSetChanged();
    }

    class GifViewHolder extends RecyclerView.ViewHolder {

        /** The placeholder colors to show when the GIF is loading. */
        private final int[] PLACEHOLDERS = {
                R.color.placeholder1,
                R.color.placeholder2,
                R.color.placeholder3,
                R.color.placeholder4,
                R.color.placeholder5
        };

        private SimpleDraweeView mSimpleDraweeView;

        GifViewHolder(View itemView) {
            super(itemView);
            mSimpleDraweeView = (SimpleDraweeView) itemView;
        }

        void bind(final Image image, final int position) {
            GenericDraweeHierarchy hierarchy = GenericDraweeHierarchyBuilder.newInstance(itemView.getResources())
                    .setPlaceholderImage(PLACEHOLDERS[position % PLACEHOLDERS.length])
                    .build();

            // Set dimensions of target view. Necessary for Fresco.
            float widthRatio = mColumnWidth / ((float) image.width);
            mSimpleDraweeView.getLayoutParams().width = mColumnWidth;
            mSimpleDraweeView.getLayoutParams().height = (int) (image.height * widthRatio);
            mSimpleDraweeView.setHierarchy(hierarchy);

            // Load the image into the target view
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(image.webp)
                    .setAutoPlayAnimations(true)
                    .build();
            mSimpleDraweeView.setController(controller);

            // Set a click listener to trigger the callback
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemCallback.onItemClicked(image);
                }
            });
        }

        void recycle() {
            itemView.setOnClickListener(null);
        }
    }

    public interface ItemCallback {
        /**
         * Triggered when the GIF has been clicked.
         * @param image The model representing the GIF.
         */
        void onItemClicked(Image image);
    }
}
