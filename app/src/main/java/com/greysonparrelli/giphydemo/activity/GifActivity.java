package com.greysonparrelli.giphydemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.greysonparrelli.giphydemo.R;
import com.greysonparrelli.giphydemo.model.Image;

import io.realm.Realm;

import static android.content.Intent.ACTION_SEND;
import static android.content.Intent.EXTRA_TEXT;

/**
 * Activity that shows a full-screen version of a GIF.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class GifActivity extends AppCompatActivity {

    public static final String KEY_IMAGE = "key_image";

    private Image mImage;
    private SimpleDraweeView mGifView;
    private Realm mRealm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif);

        // Ensure that an Image model was passed to us
        if (getIntent() == null || !getIntent().hasExtra(KEY_IMAGE)) {
            throw new IllegalStateException("Must be provided an Image through the intent.");
        }
        mImage = getIntent().getParcelableExtra(KEY_IMAGE);

        // Get the view from the layout
        mGifView = (SimpleDraweeView) findViewById(R.id.image);

        // Size the GIF view appropriately. Necessary for Fresco.
        int width = getResources().getDisplayMetrics().widthPixels;
        mGifView.getLayoutParams().width = width;
        mGifView.getLayoutParams().height = (int)(mImage.height * (width / (float) mImage.width));

        // Show the GIF
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(mImage.webp)
                .setAutoPlayAnimations(true)
                .build();
        mGifView.setController(controller);

        // Grab our Realm instance to read from peristent storage
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gif, menu);
        final MenuItem favoriteItem = menu.findItem(R.id.favorite);
        final MenuItem shareItem = menu.findItem(R.id.share);

        // Set the favorite icon appropriately (based on whether this image has been favorited)
        if (isImageFavorited()) {
            favoriteItem.setIcon(R.drawable.ic_favorite_white_24dp);
        } else {
            favoriteItem.setIcon(R.drawable.ic_favorite_border_white_24dp);
        }

        // Setup the favorites button
        favoriteItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mRealm.beginTransaction();
                if (isImageFavorited()) {
                    // Remove from favorites
                    getRealmImage().deleteFromRealm();
                    favoriteItem.setIcon(R.drawable.ic_favorite_border_white_24dp);
                } else {
                    // Add to favorites
                    mRealm.copyToRealm(mImage);
                    favoriteItem.setIcon(R.drawable.ic_favorite_white_24dp);
                }
                mRealm.commitTransaction();
                return true;
            }
        });

        // Setup the share button
        ShareActionProvider provider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        Intent shareIntent = new Intent(ACTION_SEND);
        shareIntent.putExtra(EXTRA_TEXT, mImage.url);
        shareIntent.setType("text/plain");
        provider.setShareIntent(shareIntent);

        return true;
    }

    /**
     * Build a properly-formed intent for launching this activity.
     * @param context A {@link Context}.
     * @param image The {@link Image} that should be shown in this activity.
     */
    public static Intent buildIntent(Context context, Image image) {
        Intent intent = new Intent(context, GifActivity.class);
        intent.putExtra(KEY_IMAGE, image);
        return intent;
    }

    /**
     * @return True if the {@link Image} has been favorited, otherwise false.
     */
    private boolean isImageFavorited() {
        return  getRealmImage() != null;
    }

    /**
     * @return The Realm-linked version of the {@link Image} model.
     */
    private Image getRealmImage() {
        return mRealm.where(Image.class).equalTo("url", mImage.url).findFirst();
    }
}
