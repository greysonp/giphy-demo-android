package com.greysonparrelli.giphydemo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.greysonparrelli.giphydemo.GifAdapter;
import com.greysonparrelli.giphydemo.R;
import com.greysonparrelli.giphydemo.model.Image;

import io.realm.Realm;

/**
 * Activity that shows the GIFs that the user has favorited.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class FavoritesActivity extends AppCompatActivity implements GifAdapter.ItemCallback {

    /** The RecyclerView that renders the GIFs. */
    private RecyclerView mGifList;

    /** The adapter that binds models to the views that will be presented in the GIF RecyclerView. */
    private GifAdapter mGifAdapter;

    /** A TextView that displays status messages on the screen. */
    private TextView mMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        // Find the views in the layout
        mMessage = (TextView) findViewById(R.id.message);
        mGifList = (RecyclerView) findViewById(R.id.gif_list);

        // Setup the adapter for the GIF list
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mGifAdapter = new GifAdapter(getResources().getDisplayMetrics().widthPixels / 2, this);
        mGifList.setLayoutManager(layoutManager);
        mGifList.setAdapter(mGifAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGifAdapter.addImages(Realm.getDefaultInstance().where(Image.class).findAll());

        // Show "no favorites" message when appropriate
        if (mGifAdapter.getItemCount() == 0) {
            mMessage.setVisibility(View.VISIBLE);
        } else {
            mMessage.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGifAdapter.clearImages();
    }

    @Override
    public void onItemClicked(Image image) {
        startActivity(GifActivity.buildIntent(this, image));
    }
}
