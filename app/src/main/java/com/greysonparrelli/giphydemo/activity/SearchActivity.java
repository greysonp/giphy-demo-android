package com.greysonparrelli.giphydemo.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.greysonparrelli.giphydemo.GifAdapter;
import com.greysonparrelli.giphydemo.InfiniteScrollListener;
import com.greysonparrelli.giphydemo.R;
import com.greysonparrelli.giphydemo.model.Gif;
import com.greysonparrelli.giphydemo.model.Image;
import com.greysonparrelli.giphydemo.model.Pagination;
import com.greysonparrelli.giphydemo.model.SearchResult;
import com.greysonparrelli.giphydemo.network.GiphyApi;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the first activity you'll see when you start the app. It provides the ability to search for GIFs.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class SearchActivity extends AppCompatActivity implements InfiniteScrollListener.Callback, GifAdapter.ItemCallback {

    /** The number of GIFs we want to load in each request. */
    private static final int GIF_COUNT = 25;

    /** The RecyclerView that renders the GIFs. */
    private RecyclerView mGifList;

    /** The adapter that binds models to the views that will be presented in the GIF RecyclerView. */
    private GifAdapter mGifAdapter;

    /** A TextView that displays status messages on the screen. */
    private TextView mMessage;

    /** Whether or not we are loading image results. */
    private boolean mIsLoading;

    /** The active search query. Needs to be stored for pagination. */
    private String mQuery;

    /** The pagination information. */
    private Pagination mPagination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Find the views in the layout
        mMessage = (TextView) findViewById(R.id.message);
        mGifList = (RecyclerView) findViewById(R.id.gif_list);

        // Setup the adapter for the GIF list
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mGifAdapter = new GifAdapter(getResources().getDisplayMetrics().widthPixels / 2, this);
        mGifList.setLayoutManager(layoutManager);
        mGifList.setAdapter(mGifAdapter);
        mGifList.addOnScrollListener(new InfiniteScrollListener(layoutManager, GIF_COUNT / 3, this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchViewItem = menu.findItem(R.id.search);
        MenuItem favoritesItem = menu.findItem(R.id.favorites);

        // Setup the search view
        final SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_hint));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                mQuery = query;
                loadImages(query, GIF_COUNT, 0, true);
                searchView.clearFocus();
                return true;
            }
        });

        // Setup the favorites button
        favoritesItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                startActivity(new Intent(SearchActivity.this, FavoritesActivity.class));
                return true;
            }
        });

        return true;
    }

    @Override
    public void onEndOfList() {
        int lastLoaded = mPagination.offset + mPagination.count;
        if (!mIsLoading && lastLoaded < mPagination.total_count) {
            loadImages(mQuery, GIF_COUNT, lastLoaded, false);
        }
    }

    @Override
    public void onItemClicked(Image image) {
        startActivity(GifActivity.buildIntent(this, image));
    }

    private void loadImages(String query, int limit, int offset, final boolean clearOldData) {
        mIsLoading = true;
        GiphyApi.getInstance().search(query, limit, offset, new GiphyApi.SearchCallback() {
            @Override
            public void onResult(SearchResult result) {
                mIsLoading = false;
                mPagination = result.pagination;

                List<Image> images = new ArrayList<>(result.data.size());
                for(Gif gif : result.data) {
                    images.add(gif.images.fixed_width);
                }
                if (clearOldData) {
                    mGifAdapter.clearImages();
                    mGifList.scrollToPosition(0);

                    // Bug with StaggeredGridLayoutManager
                    // See: http://stackoverflow.com/questions/33126960/android-staggeredgridlayoutmanager-offset-bug
                    mGifList.getLayoutManager().onDetachedFromWindow(mGifList, null);
                }
                mGifAdapter.addImages(images);

                // Show the correct message to the user depending on whether we got results
                if (mGifAdapter.getItemCount() == 0) {
                    mMessage.setVisibility(View.VISIBLE);
                    mMessage.setText(getString(R.string.no_results));
                } else {
                    mMessage.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                mIsLoading = false;
                Toast.makeText(SearchActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
