package com.greysonparrelli.giphydemo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Scroll listener that triggers a callback when the user has scrolled to the bottom of the list.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class InfiniteScrollListener extends RecyclerView.OnScrollListener {

    /** The layout manager of the list we're watching. */
    private final StaggeredGridLayoutManager mLayoutManager;

    /** How many items before the bottom of the list that you want the callback to trigger. */
    private final int mBuffer;

    /** The callback that will trigger when you reach the bottom of the list. */
    private final Callback mCallback;

    /**
     * Needed for {@link StaggeredGridLayoutManager#findLastVisibleItemPositions(int[])}. Kept as a member to avoid
     * constant re-allocations.
     */
    private final int[] mSpan;

    /**
     * Creates a new {@link InfiniteScrollListener}.
     * @param layoutManager The layout manager for the list you're listening to.
     * @param buffer How many items before the bottom of the list that you want the callback to trigger.
     * @param callback The callback that will trigger when you reach the bottom of the list.
     */
    public InfiniteScrollListener(
            @NonNull StaggeredGridLayoutManager layoutManager,
            int buffer,
            @NonNull Callback callback) {
        mLayoutManager = layoutManager;
        mBuffer = buffer;
        mCallback = callback;
        mSpan = new int[mLayoutManager.getSpanCount()];
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            mLayoutManager.findLastVisibleItemPositions(mSpan);
            if (mSpan[0] >= mLayoutManager.getItemCount() - mBuffer) {
                mCallback.onEndOfList();
            }
        }
    }

    public interface Callback {
        /**
         * Triggered when the bottom of the list has been reached (minus any specified buffer).
         */
        void onEndOfList();
    }
}
