package com.greysonparrelli.giphydemo.model;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class Pagination {
    public int total_count;
    public int count;
    public int offset;
}
