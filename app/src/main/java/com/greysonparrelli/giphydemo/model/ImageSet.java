package com.greysonparrelli.giphydemo.model;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class ImageSet {
    public Image fixed_height;
    public Image fixed_width;
    public Image fixed_width_downsampled;
    public Image original;
}
