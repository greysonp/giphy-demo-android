package com.greysonparrelli.giphydemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class Image extends RealmObject implements Parcelable {

    @PrimaryKey
    public String url;
    public int width;
    public int height;
    public long size;
    public String webp;
    public long webp_size;
    public String mp4;

    public Image() {

    }

    private Image(Parcel in) {
        url = in.readString();
        width = in.readInt();
        height = in.readInt();
        size = in.readLong();
        webp = in.readString();
        webp_size = in.readLong();
        mp4 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeInt(width);
        parcel.writeInt(height);
        parcel.writeLong(size);
        parcel.writeString(webp);
        parcel.writeLong(webp_size);
        parcel.writeString(mp4);
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
