package com.greysonparrelli.giphydemo.model;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class Gif {
    public String id;
    public String slug;
    public String url;
    public String username;
    public ImageSet images;
}
