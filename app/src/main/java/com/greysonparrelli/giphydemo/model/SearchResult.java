package com.greysonparrelli.giphydemo.model;

import java.util.List;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class SearchResult {
    public List<Gif> data;
    public Pagination pagination;
}
