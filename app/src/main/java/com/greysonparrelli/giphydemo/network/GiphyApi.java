package com.greysonparrelli.giphydemo.network;

import android.support.annotation.NonNull;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.greysonparrelli.giphydemo.BuildConfig;
import com.greysonparrelli.giphydemo.model.SearchResult;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A convenience class make requests against the Giphy API.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class GiphyApi {

    /** The singleton instance. */
    private static final GiphyApi sInstance = new GiphyApi();

    /** The {@link Retrofit} service that we're wrapping. */
    private final GiphyService mGiphyService;

    /**
     * @return A singleton instance of the class.
     */
    public static GiphyApi getInstance() {
        return sInstance;
    }

    private GiphyApi() {
        // Add a network interceptor that adds the Giphy API key to every request
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                // We're using the public dev key. If this were to be used for any significant amount of users, you'd
                // have to request a production API key from Giphy.
                HttpUrl.Builder builder = originalHttpUrl.newBuilder()
                        .addQueryParameter("api_key", "dc6zaTOxFJmzC");

                Request.Builder requestBuilder = original.newBuilder()
                        .url(builder.build())
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        // Add Stetho support on debug builds
        if (BuildConfig.DEBUG) {
            clientBuilder.addNetworkInterceptor(new StethoInterceptor());
        }

        // Initialize the Retrofit service we're wrapping
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.giphy.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build();
        mGiphyService = retrofit.create(GiphyService.class);
    }

    /**
     * Send a request for a search term.
     * @param query The search query.
     * @param limit How many items to return in the result.
     * @param offset How many items to skip in the search result set.
     * @param callback The callback that will be triggered when the results are ready.
     */
    public void search(final String query, int limit, int offset, @NonNull final SearchCallback callback) {
        mGiphyService.search(query, limit, offset).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, retrofit2.Response<SearchResult> response) {
                callback.onResult(response.body());
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public interface SearchCallback {
        /**
         * Triggered when the search results are ready.
         * @param result The search results.
         */
        void onResult(SearchResult result);

        /**
         * Triggered when the request fails.
         * @param throwable The error that occurred during the request.
         */
        void onFailure(Throwable throwable);
    }
}
