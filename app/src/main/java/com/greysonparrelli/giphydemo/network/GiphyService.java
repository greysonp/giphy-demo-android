package com.greysonparrelli.giphydemo.network;

import com.greysonparrelli.giphydemo.model.SearchResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * A {@link retrofit2.Retrofit} service that handles interaction with the Giphy REST API.w
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
interface GiphyService {

    @GET("gifs/search")
    Call<SearchResult> search(@Query("q") String query, @Query("limit") int limit, @Query("offset") int offset);
}
