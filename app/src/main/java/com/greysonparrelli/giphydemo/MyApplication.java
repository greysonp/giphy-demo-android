package com.greysonparrelli.giphydemo;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import io.realm.Realm;

/**
 * Application class that lets us initialize our libraries when the app starts.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Fresco.initialize(this);
    }
}
