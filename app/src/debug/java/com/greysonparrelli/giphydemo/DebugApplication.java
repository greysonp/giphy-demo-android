package com.greysonparrelli.giphydemo;

import com.facebook.stetho.Stetho;

/**
 * A debug version of our Application that initializes debug-specific tools, like Stetho.
 *
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class DebugApplication extends MyApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
