# Giphy Sample App

This is intended to be a simple app that lets you search and save GIFs for easy sharing.

It's intended to be used mostly as a learning tool and serve as a demonstration for various common building blocks in Android apps.
